# BDD Example for boisset-dev

## Usage

- **Install mocha and phantomjs globally**

```sh
$ npm install mocha -g
$ npm install phantomjs -g
```

- **Install other dependencies**

```sh
$ npm install
```

- **Run tests**

```sh
$ npm test
```

or, if you only want to run one of the tests (I only wrote one. However, in reality, you might have 200+ tests here), you can do

```sh
$ cd tests
$ mocha order.js
```
