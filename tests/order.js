var chai = require('chai');
//A high level wrapper for Phantomjs. http://nightmarejs.org
var Nightmare = require('nightmare');
var ROOT_URL = 'http://boisset-dev.gsati.net';

chai.should();

describe('Order process', function(){
  var nightmare = new Nightmare({cookiesFile: './tests/cookies_order.txt'});
  var page;
  context('when at home page', function(){
    beforeEach(function(){
      page = nightmare
      .viewport(1920, 1080)
      .goto(ROOT_URL)
    });
    it('should be able to link to shop page', function(done){
      //otherwise test will fail because of timeout, since phantomjs is not that fase
      //you can also set timeout using --timeout on cli, but then it'll be global
      this.timeout(30000);
      page
      .click('a.menu-shop')
      .wait('document')
      //phantomjs doesn't recognize web fonts by default, but there is some fancy plugin that does that
      .screenshot('./tests/shop_page.jpg')
      .evaluate(function(){
        return document.URL;
      }, function(docURL){
        docURL.should.equal(ROOT_URL + '/products/catalogs/wine');

      })
      //done is a promise. used it because .run is async
      .run(done);
    })
  });
  context('when at shop page - quick view menu', function(){
    beforeEach(function(){
      page = nightmare
      .viewport(1920, 1080)
      .goto(ROOT_URL + '/products/catalogs/wine')
      .evaluate(function(){
        var element = document.querySelector('article.product-list:first-child');
        var event = document.createEvent('MouseEvent');
        //TODO: test: submitted fork request for mouseover event. ironically, owner asked me to write a test.
        event.initMouseEvent('mouseover', true, true);
        element.dispatchEvent(event);
      })
    });

    it('quick add menu should display on mouseover', function(done){
      this.timeout(30000);
      page
      .screenshot('./tests/quick_add_menu_on_shop_page.jpg')
      .visible('div.quick-view-choices', function(isVisible){
        isVisible.should.equal(true);
      })
      .run(done);
    });

    it('should display a confirmation after clicking add to cart,' +
    'where the go to checkout button will take me to checkout screen', function(done){
      this.timeout(30000);
      page.click('li.button-addtocart')
      .screenshot('./tests/quick_add_confirmation_on_shop_page.jpg')
      .visible('div#commerce-add-to-cart-popup', function(isVisible){
        isVisible.should.equal(true);
      })
      .wait("div.button > a[href='/cart']")
      .click("div.button > a[href='/cart']")
      .wait()
      .screenshot('./tests/checkout_1.jpg')
      .url(function(cartURL){
        cartURL.should.equal(ROOT_URL + '/cart');
      })
      .run(done);
    })
  })
})
